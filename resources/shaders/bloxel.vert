#version 430 core
layout (location = 0) in vec3 inPos; //3 bytes
layout (location = 1) in vec2 inTextureUV; //2 bytes

uniform mat4 mvp;

out vec2 TextureCoord;
void main()
{
    gl_Position = mvp * vec4(inPos, 1.0);
    TextureCoord = inTextureUV;
}