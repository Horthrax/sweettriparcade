#version 430 core
out vec4 FragColor;

uniform sampler2DArray textureArray;
in vec2 TextureCoord;
uniform int layer;
uniform vec3 color;

void main()
{
    FragColor = texture(textureArray, vec3(TextureCoord, layer));
    if(color.z != 0){
        FragColor = mix(FragColor, vec4(color, 1.0), 0.9);
    }
}