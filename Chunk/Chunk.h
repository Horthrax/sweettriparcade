//
// Created by Horthrax on 8/23/2019.
//

#ifndef SWEETTRIPARCADE_CHUNK_H
#define SWEETTRIPARCADE_CHUNK_H

#include "../Block/Block.h";

struct Chunk {
    int x, y, z;
};

#endif //SWEETTRIPARCADE_CHUNK_H
