//
// Created by Horthrax on 8/19/2019.
//

#ifndef SWEETTRIPARCADE_BLOCK_H
#define SWEETTRIPARCADE_BLOCK_H

struct CubeAttribute{
    CubeAttribute(unsigned char x, unsigned char y, unsigned char z, unsigned char u, unsigned char v) {
        position[0] = x;
        position[1] = y;
        position[2] = z;

        texture[0] = u;
        texture[1] = v;
    }

    unsigned char position[3];
    unsigned char texture[2];
};

struct Block {
    unsigned short id;
};

enum POWER_UP{ OIL};

struct PowerUp{
    POWER_UP type;
    Vec3f pos;
    bool visible;
};
struct Machine : Block {
    bool on;
};

#endif //SWEETTRIPARCADE_BLOCK_H
