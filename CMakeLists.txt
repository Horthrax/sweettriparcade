cmake_minimum_required(VERSION 3.7)
project(SweetTripArcade)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

find_package(SDL2 REQUIRED)
find_package(GLEW REQUIRED)

include_directories(${SDL2_INCLUDE_DIR} ${GLEW_INCLUDE_DIRS})

add_executable(SweetTripArcade main.cpp InputHandling/InputHandler.cpp InputHandling/InputHandler.h Rendering/Renderer.cpp Rendering/Renderer.h Util/FileHandler.h Rendering/Shader.cpp Rendering/Shader.h Block/Block.h Rendering/VBOWrapper.h Util/FileHandler.cpp Util/Util.h Chunk/Chunk.h)
target_link_libraries(SweetTripArcade ${SDL2_LIBRARY} ${GLEW_LIBRARIES} -lmingw32 -lglew32 -lglu32 -lopengl32 -lSDL2main -lSDL2 -mwindows)