//
// Created by Horthrax on 8/19/2019.
//

#ifndef SWEETTRIPARCADE_INPUTHANDLER_H
#define SWEETTRIPARCADE_INPUTHANDLER_H


#include <SDL_events.h>
#include <iostream>
#pragma once
#include "../Util/Util.h"
#include <vec3.hpp>
#include <math.h>

class InputHandler {
public:
    InputHandler();
    InputHandler(Rotator* que);
    ~InputHandler();
    bool window_context;
    void handle_input(bool* quit);
private:
    bool w = false, a = false, s = false, d = false;
    Rotator* que = {};
};

#endif //SWEETTRIPARCADE_INPUTHANDLER_H
