//
// Created by Horthrax on 8/19/2019.
//

#include "InputHandler.h"

void InputHandler::handle_input(bool *quit){
    SDL_Event event;
    while(SDL_PollEvent(&event)){
        switch(event.type) {
            case(SDL_QUIT):
                *quit = true;
                break;
            case (SDL_KEYDOWN):
                switch (event.key.keysym.sym) {
                    case(SDLK_a):
                        a = true;
                        break;
                        break;
                    case(SDLK_d):
                        d = true;
                        break;
                        break;
                    case(SDLK_s):
                        s = true;
                        break;
                    case(SDLK_w):
                        w = true;
                        break;
                    case(SDLK_SPACE):
                        //inversion = !inversion;
                        break;
                    case(SDLK_LCTRL):
                        break;
                    case SDLK_ESCAPE:
                        window_context = !window_context;
                        if (window_context) {
                            SDL_ShowCursor(0);
                            SDL_SetRelativeMouseMode(SDL_TRUE);
                        } else {
                            SDL_ShowCursor(1);
                            SDL_SetRelativeMouseMode(SDL_FALSE);
                        }
                        break;
                }
                break;
            case (SDL_KEYUP):
                switch (event.key.keysym.sym) {
                    case(SDLK_a):
                        a = false;
                        break;
                        break;
                    case(SDLK_d):
                        d = false;
                        break;
                        break;
                    case(SDLK_s):
                        s = false;
                        break;
                    case(SDLK_w):
                        w = false;
                        break;
                    case(SDLK_SPACE):
                        //inversion = !inversion;
                        break;
                    case(SDLK_LCTRL):
                        break;
                    case SDLK_ESCAPE:
                        window_context = !window_context;
                        if (window_context) {
                            SDL_ShowCursor(0);
                            SDL_SetRelativeMouseMode(SDL_TRUE);
                        } else {
                            SDL_ShowCursor(1);
                            SDL_SetRelativeMouseMode(SDL_FALSE);
                        }
                        break;
                }
                break;
        }
    }
    if(que->isEmpty()) {
        if (w) {
            que->y = que->steps;
            return;
        }if (a) {
            que->x = -que->steps;
            return;
        }
        if (s) {
            que->y = -que->steps;
            return;
        }
        if (d) {
            que->x = que->steps;
            return;
        }
    }
}

InputHandler::InputHandler(Rotator* _que): que(_que) {}

InputHandler::InputHandler() {}

InputHandler::~InputHandler() {}