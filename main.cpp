#include <iostream>
#include <string>
#include <unordered_map>

#include "InputHandling/InputHandler.h"
#include "Rendering/Renderer.h"

bool quit = false;

using namespace std;

int main(int argc, char *argv[]) {
    Renderer renderer = Renderer();
    InputHandler input_handler = InputHandler(&renderer.que);
    renderer.init();

    //RLEInterpreter interpreter = RLEInterpreter(".sts");
    //interpreter.interpret("asdfadf.sts");

    while(!quit){
        input_handler.handle_input(&quit);
        renderer.render();
    }
    renderer.close();
    return 0;
}