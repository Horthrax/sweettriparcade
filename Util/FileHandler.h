//
// Created by Horthrax on 8/19/2019.
//

#ifndef SWEETTRIPARCADE_FILEHANDLER_H
#define SWEETTRIPARCADE_FILEHANDLER_H

#include <Windows.h>
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include "Util.h"
struct Image
{
    int ID, width, height, nrChannels;
    unsigned char* data;
};

class FileHandler {
public:
    static std::string loadStrFile(const char * path);
    static std::vector<std::string> getFilesAt(std::string folder);
    static std::vector<std::string> getFilesAt(std::string folder, std::string ends_with);
    static Image loadImage(std::string filename, int channels = 3);
};


#endif //SWEETTRIPARCADE_FILEHANDLER_H
