//
// Created by Horthrax on 8/23/2019.
//

#ifndef SWEETTRIPARCADE_UTIL_H
#define SWEETTRIPARCADE_UTIL_H
#include <string>
#include <iostream>

struct Rotator{
    int x, y, z;
    int steps;
    bool isEmpty(){
        //return !x && !y && !z;
        return !(x+y+z);
    }
};

static bool string_ends_with(std::string string, std::string ends_with){
    size_t string_length = string.length();
    size_t compared_length = string_length - ends_with.length();
    for(size_t i = compared_length; i < string_length; i++){
        if(string[i] != ends_with[i-compared_length]){
            return false;
        }
    }
    return true;
}

struct Vec3i
{
    int x, y, z;
    size_t operator()(const Vec3i& pointToHash) const noexcept {
        size_t h1 = std::hash<double>()(pointToHash.x);
        size_t h2 = std::hash<double>()(pointToHash.y);
        size_t h3 = std::hash<double>()(pointToHash.z);
        return (h1 ^ (h2 << 1)) ^ (h3 << 2);
    };
    bool operator==(const Vec3i& other) const {
        return (x == other.x && y == other.y && z == other.z);
    };
    bool operator!=(const Vec3i& other) const {
        return (x != other.x && y != other.y && z != other.z);
    };
    bool operator<(const Vec3i& rhs) const
    {
        return (x < rhs.x && y < rhs.y && z < rhs.z);
    }
    bool operator>(const Vec3i& rhs) const
    {
        return (x > rhs.x && y > rhs.y && z > rhs.z);
    }
    friend std::ostream& operator << (std::ostream& cout, const Vec3i& rhs)
    {
        cout << "X:" << rhs.x << " Y: " << rhs.y << " Z: " << rhs.z;
        return cout;
    }
};



struct Vec3f
{
    float x, y, z;

    bool operator==(const Vec3f& other) const {
        return (x == other.x && y == other.y && z == other.z);
    };
    bool operator!=(const Vec3f& other) const {
        return (x != other.x && y != other.y && z != other.z);
    };
    bool operator<(const Vec3f& rhs) const
    {
        return (x < rhs.x && y < rhs.y && z < rhs.z);
    }
    bool operator>(const Vec3f& rhs) const
    {
        return (x > rhs.x && y > rhs.y && z > rhs.z);
    }
    friend std::ostream& operator << (std::ostream& cout, const Vec3f& rhs)
    {
        cout << "X:" << rhs.x << " Y: " << rhs.y << " Z: " << rhs.z;
        return cout;
    }
};

struct Vec3d
{
    double x, y, z;

    bool operator==(const Vec3d& other) const {
        return (x == other.x && y == other.y && z == other.z);
    };
    bool operator!=(const Vec3d& other) const {
        return (x != other.x && y != other.y && z != other.z);
    };
    bool operator<(const Vec3d& rhs) const
    {
        return (x < rhs.x && y < rhs.y && z < rhs.z);
    }
    bool operator>(const Vec3d& rhs) const
    {
        return (x > rhs.x && y > rhs.y && z > rhs.z);
    }
    Vec3d operator*(const int rhs) const
    {
        return Vec3d{ x * rhs, y * rhs, z * rhs };
    }
    Vec3d operator*=(const int rhs)
    {
        x *= rhs;
        y *= rhs;
        z *= rhs;
        return *this;
    }
    Vec3d operator*=(const float rhs)
    {
        x *= rhs;
        y *= rhs;
        z *= rhs;
        return *this;
    }
    friend std::ostream& operator << (std::ostream& cout, const Vec3d& rhs)
    {
        cout << "X:" << rhs.x << " Y: " << rhs.y << " Z: " << rhs.z;
        return cout;
    }
};

#endif //SWEETTRIPARCADE_UTIL_H