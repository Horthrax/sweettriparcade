//
// Created by Horthrax on 8/20/2019.
//

#include "FileHandler.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

std::string FileHandler::loadStrFile(const char * path){
    std::ifstream file(path, std::ios::binary | std::ios::ate);
    if (!file.is_open()) {
        std::cout << "Could not read file at " << path << "!" << std::endl;
    }
    std::string source;
    source.resize(file.tellg());
    file.seekg(0, std::ios::beg);
    file.read(&source[0], source.size());

    std::cout << "Loaded file: " << path << std::endl;

    return source;
}

std::vector<std::string> FileHandler::getFilesAt(std::string folder)
{
    std::vector<std::string> names;
    std::string search_path = folder + "/*.*";
    std::cout << "Path: " << search_path << "\n";
    WIN32_FIND_DATA fd;
    HANDLE hFind = ::FindFirstFile(search_path.c_str(), &fd);
    if (hFind != INVALID_HANDLE_VALUE) {
        do {
            // read all (real) files in current folder
            // , delete '!' read other 2 default folder . and ..
            if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
                names.push_back(fd.cFileName);
                //std::cout << fd.cFileName << "\n";
            }
        } while (::FindNextFile(hFind, &fd));
        ::FindClose(hFind);
    }
    return names;
}
std::vector<std::string> FileHandler::getFilesAt(std::string folder, std::string ends_with)
{
    std::vector<std::string> names;
    std::string search_path = folder + "/*.*";
    std::cout << "Path: " << search_path << "\n";
    WIN32_FIND_DATA fd;
    HANDLE hFind = ::FindFirstFile(search_path.c_str(), &fd);
    if (hFind != INVALID_HANDLE_VALUE) {
        do {
            // read all (real) files in current folder
            // , delete '!' read other 2 default folder . and ..
            if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
                if(string_ends_with(std::string(fd.cFileName), ends_with)){
                    names.push_back(fd.cFileName);
                }
                //std::cout << fd.cFileName << "\n";
            }
        } while (::FindNextFile(hFind, &fd));
        ::FindClose(hFind);
    }
    return names;
}


Image FileHandler::loadImage(std::string filename, int channels){
    Image image;
    const char* file = filename.c_str();
    int width, height, nrChannels;
    image.data = stbi_load(file, &width, &height, &nrChannels, channels);
    image.width = width;
    image.height = height;
    image.nrChannels = nrChannels;
    if (!image.data)
        std::cout << "Failed to load texture: " << filename << std::endl;
    else
        std::cout << "Loaded texture: " << filename << " (w:" << width << ", h:" << height << ")" << std::endl;
    return image;
}
