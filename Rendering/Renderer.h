//
// Created by Horthrax on 8/19/2019.
//

#ifndef SWEETTRIPARCADE_RENDERER_H
#define SWEETTRIPARCADE_RENDERER_H

#pragma once
#include <iostream>
#include <algorithm>

#define GLEW_STATIC
#include "GL/glew.h"
#include "GL/glu.h"

#include "SDL.h"
#include "SDL_opengl.h"

#include "Shader.h"
#include "VBOWrapper.h"

#include "glm.hpp"

#include "../Block/Block.h"

#include "../Util/FileHandler.h"
#include "../Util/Util.h"

class Renderer {
public:
    Renderer();
    ~Renderer();
    void init();
    void close();
    void render();
    Rotator que = {0,0,0,20};
    Vec3i pos = {};
private:
    SDL_Window *window = NULL;
    SDL_GLContext gl_context;


    Shader bloxel_shader;
    Shader color_shader;
    Shader transparent_shader;

    const float COLOR_OPENGL = 1.0/255;

    glm::vec3 player_color = {0,0,0};

    unsigned int texture_array;
    unsigned int VAO, VBO, cube_VAO;

    float rotation_amount = (float)90/que.steps; //90/que.steps

    const int WINDOW_WIDTH = 1920; // 1920 x 1080
    const int WINDOW_HEIGHT = 1080;
    const float Y_OFFSET = 25.0f; //15
    const int QUE_PRECISION = 100;
    const char *WINDOW_TITLE = "Title";

    int layer_count = 0;
    std::string final_data;

    bool init_SDL();
    void init_OpenGL();
    void init_textures();
    void init_objects();

    std::vector<Vec3f> random_objects = {};
    std::vector<PowerUp> power_ups = {};

    void render_player();
    void render_enemies();
    void render_borders();
};


#endif //SWEETTRIPARCADE_RENDERER_H
