//
// Created by Horthrax on 8/19/2019.
//

#include <ext/matrix_transform.hpp>
#include <gtc/matrix_transform.hpp>
#include "Renderer.h"

using std::cout;
using std::endl;

Renderer::Renderer(){}
Renderer::~Renderer(){}

void Renderer::init(){
    if(init_SDL()){
        init_OpenGL();
    }
    init_objects();
}

void Renderer::init_objects(){
    // Arbitrary power-ups
    /*
     * POWER_UP type;
    Vec3f pos;
    bool visible;
     */

    for(int i = 0; i < 12; i++){
        power_ups.push_back({OIL, {(float)(rand()%20), (float)(rand()%20), (float)(rand()%20)}, true});
    }
    // Scene
    for(int i = 0; i < 20; i++){
        for(int j = 0; j < 20; j++){
            random_objects.push_back({(float)i, (float)j, (float)j});
        }
    }
}

bool Renderer::init_SDL(){
    if(SDL_Init(SDL_INIT_VIDEO) < 0){
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
    } else {
        SDL_SetRelativeMouseMode(SDL_TRUE);
        SDL_ShowCursor(0);

        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
        SDL_GL_SetSwapInterval(0);

        //Antialiasing
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2);

        glEnable(GL_MULTISAMPLE);
        window = SDL_CreateWindow(WINDOW_TITLE, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
        if(window == NULL){
            printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        } else {
            SDL_SetWindowResizable(window, SDL_TRUE);
            gl_context = SDL_GL_CreateContext(window);

            if(gl_context == NULL){
                printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
            }else{
                printf("Window: %s was created!\n", WINDOW_TITLE);
                return true;
            }
        }
    }
    return false;
}

void Renderer::init_OpenGL(){
    glewExperimental = GL_TRUE;
    glewInit();

    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_CULL_FACE);
    glClearColor(0.2f, 0.3f, 0.4f, 1.0f);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

    // Wire frame
    //glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );

    bloxel_shader = Shader("bloxel");
    color_shader = Shader("color");
    transparent_shader = Shader("transparent");
    CubeVBO cubeVBO = CubeVBO();
    cube_VAO = cubeVBO.VAO;

    init_textures();
}
template <typename T>
struct wrap_val{
    T val;

};
void Renderer::init_textures() {
    int width = 16;
    int height = 16;

    std::vector<std::string> texture_names = FileHandler::getFilesAt(
            "E:/Programming/SweetTripArcade/resources/textures/", ".png");
    assert(texture_names.size() > 0);
    glGenTextures(1, &texture_array);
    glBindTexture(GL_TEXTURE_2D_ARRAY, texture_array);
    std::stringstream ss;
    unsigned char* data;
    for (auto it_textures = texture_names.begin(); it_textures != texture_names.end(); it_textures++) {
        std::string texture_name = *it_textures;
        Image texture_image = FileHandler::loadImage(
                "E:/Programming/SweetTripArcade/resources/textures/" + texture_name, 3);
        if (texture_image.width == width && texture_image.height == height) {
            std::cout << "Texture found: " << texture_name << std::endl;
            //std::string data((LPCTSTR)texture_image.data);
            //std::stringstream ss;
            //ss << texture_image.data;
            //texture_im = texture_image;

            std::cout <<  "Texture image data adjusted: " << sizeof(texture_image.data) << std::endl;
            std::string lolepic(reinterpret_cast<const char*>(texture_image.data), sizeof(texture_image.data));
            final_data.append((LPTSTR)(char *)&texture_image.data);
            layer_count++;
            std::cout << "Size of final data: " << sizeof(final_data) << std::endl;
            data = texture_image.data;
        } else {
            //std::cout << "Invalid Texture found: " << texture_name << std::endl;
        }
        glTexStorage3D(GL_TEXTURE_2D_ARRAY,
                       1,                  // Mipmaps
                       GL_RGB8,           // Texture image format
                       width, height,      // Width, height
                       layer_count               // Number of layers
        );
        glTexSubImage3D(GL_TEXTURE_2D_ARRAY,
                        0,
                        0, 0,
                        0,
                        width, height, layer_count,
                        GL_RGB,
                        GL_UNSIGNED_BYTE,
                        data
        );
    }
    //final_data = ss.str();
    //final_data.append(ss.str());

    std::cout << "Amount of valid textures: " << layer_count << std::endl;
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void Renderer::close(){
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void Renderer::render(){

    //glClearColor(0.2f, 0.45f, 0.6f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    render_player();

    SDL_GL_SwapWindow(window);
}

int rotation = 0;

float val = 0.001f;
bool invert = false;
const float increment = 0.0015f;

void Renderer::render_player(){
    rotation++;
    /*
    if(val >= 0.25f || val <= 0.0f){
        invert = !invert;
    }
    invert ? val+=increment : val-=increment;
     */
    glClearColor(0.1f, 0.0f, 0.0f, 1.0f);
    //glClearColor(0.0f, val, val, 1.0f);
    glBindVertexArray(cube_VAO);
    glActiveTexture(GL_TEXTURE0);

    glm::mat4 model = glm::mat4(1.0f);
    glm::mat4 view = glm::mat4(1.0f);
    glm::mat4 projection = glm::mat4(1.0f);
    view  = glm::translate(view, glm::vec3(-0.5f, -0.5f, -Y_OFFSET));
    projection = glm::perspective(glm::radians(45.0f), (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 0.1f, 4000.0f);

    if(que.x){
        int effect = que.x > 0 ? 1 : -1;
        //int relative_steps = que.steps-abs(que.x);
        int relative_steps = que.steps-abs(que.x);
        float open_gl_step = que.x > 0 ? (1.0f/que.steps)*QUE_PRECISION : -(1.0f/que.steps)*QUE_PRECISION;
        if(que.x < 0){
            model = glm::translate(model,glm::vec3(1.0f,0.0, 0.0))
                    *glm::rotate(model, glm::radians(relative_steps*rotation_amount), glm::vec3(0.0f, effect, 0.0f))
                    *glm::translate(model,glm::vec3(-1.0f,0.0, 0.0));
        }else{
            model = glm::rotate(model, glm::radians(relative_steps*rotation_amount), glm::vec3(0.0f, effect, 0.0f));
        }
        pos.x = pos.x+open_gl_step;
        que.x -= effect;
    }
    if(que.y){
        int effect = que.y > 0 ? 1 : -1;
        int relative_steps = que.steps-abs(que.y);
        float open_gl_step = que.y > 0 ? (1.0f/que.steps)*QUE_PRECISION : -(1.0f/que.steps)*QUE_PRECISION;
        if(que.y < 0){
            model = glm::translate(model,glm::vec3(0.0f,1.0f, 0.0f))*
                    glm::rotate(model, glm::radians(relative_steps*rotation_amount), glm::vec3(-effect, 0.0f, 0.0f))*
                    glm::translate(model,glm::vec3(0.0f,-1.0f, 0.0f));
        }else{
            model = glm::rotate(model, glm::radians(relative_steps*rotation_amount), glm::vec3(-effect, 0.0f, 0.0f));
        }
        pos.y = pos.y+open_gl_step;
        que.y -= effect;
    }

    bloxel_shader.use();
    //view = glm::translate(view, glm::vec3(pos.x, pos.y, pos.z));

    /*
     * Instead of having to parse from std::string to char * every time just set a single one
     * TODO: Set this to a uniform instead of doing bad practice mentioned above
    */
    glm::mat4 mvp = projection * view * model;
    //bloxel_shader.setVec3("color", glm::vec3(0.1,0.0,0.4));
    bloxel_shader.setInt("layer", 1);
    bloxel_shader.setMat4("mvp", mvp);
    if(player_color.x != 0){
        bloxel_shader.setVec3("color", player_color);
    }


    glDrawArrays(GL_TRIANGLES, 0, sizeof(CubeAttribute)*36);
    //(program_mvp, 1, GL_FALSE, (const GLfloat*) &mvp)
    color_shader.use();
    //color_shader.setVec3("color", glm::vec3(0.0,0.3,0.3));
    for(auto it = random_objects.begin(); it != random_objects.end(); it++){
        int val = std::distance(it, random_objects.end())%2;
        color_shader.setVec3("color", glm::vec3(val,0.3,0.3));
        glm::mat4 random_view = glm::mat4(1.0);
        glm::mat4 random_model = glm::mat4(1.0f);
        glm::mat4 random_projection = glm::mat4(1.0f);
        random_view  = glm::translate(random_view, glm::vec3(-0.5f, -0.5f, -Y_OFFSET-1.0f));
        Vec3f current_pos = (*it);
        random_view = glm::translate(random_view, glm::vec3(-(pos.x/(float)QUE_PRECISION), -pos.y/(float)QUE_PRECISION, 0.0f));
        random_view = glm::translate(random_view, glm::vec3(current_pos.x-10.0f, current_pos.y-10.0f, 0.0f));
        random_projection = glm::perspective(glm::radians(45.0f), (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 0.1f, 4000.0f);

        color_shader.setMat4("mvp", random_projection * random_view * random_model);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(CubeAttribute)*36);
    }
    transparent_shader.use();
    transparent_shader.setVec3("color", glm::vec3(val,0.7,0.3));
    transparent_shader.setFloat("transparency", 0.3f);
    for(auto it = power_ups.begin(); it != power_ups.end(); it++){
        PowerUp power_up = (*it);
        if(!power_up.visible){
            continue;
        }
        Vec3f power_pos = power_up.pos;
        transparent_shader.setVec3("color", glm::vec3(val,0.7,0.3));
        float okay_duuude = 0;
        //TODO: Change the -10s to a constant value of the board size aka offset
        if((power_pos.x - pos.x/QUE_PRECISION)-10 == 0 && (power_pos.y - pos.y/QUE_PRECISION)-10 == 0){
            okay_duuude = 2.0;
            transparent_shader.setVec3("color", glm::vec3(0, 0.0, 1.0));
            switch(power_up.type){
                case OIL:
                    player_color = glm::mix(player_color, glm::vec3(0.639f, 0.399f, 0.231f), 0.5f);
                    break;
            }
            (*it).visible = false;
        }
        glm::mat4 random_view = glm::mat4(1.0);
        glm::mat4 random_model = glm::mat4(1.0f);
        glm::mat4 random_projection = glm::mat4(1.0f);
        random_view  = glm::translate(random_view, glm::vec3(-0.5f, -0.5f, -Y_OFFSET+okay_duuude));
        random_view = glm::translate(random_view, glm::vec3(-pos.x/(float)QUE_PRECISION, -pos.y/(float)QUE_PRECISION, 0.0f));
        random_view = glm::translate(random_view, glm::vec3(power_pos.x-10.0f, power_pos.y-10.0f, 0.0f));
        random_projection = glm::perspective(glm::radians(45.0f), (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 0.1f, 4000.0f);

        transparent_shader.setMat4("mvp", random_projection * random_view * random_model);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(CubeAttribute)*36);
    }

    //glDrawArrays(GL_TRIANGLES, 0, sizeof(CubeAttribute)*36);
}
void Renderer::render_enemies(){}
void Renderer::render_borders(){}