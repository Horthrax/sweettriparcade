//
// Created by Horthrax on 8/19/2019.
//

#include "Shader.h"

Shader::Shader(){}
Shader::~Shader(){}

Shader::Shader(std::string name) {
    std::string vertex_path = "E:/Programming/SweetTripArcade/resources/shaders/" + name + ".vert";
    std::string fragment_path = "E:/Programming/SweetTripArcade/resources/shaders/" + name + ".frag";

    std::string vertex_code = FileHandler::loadStrFile(vertex_path.data());
    std::string fragment_code = FileHandler::loadStrFile(fragment_path.data());

    GLint vertex_size = vertex_code.size();
    GLint fragment_size = fragment_code.size();

    const char *vertex_code_c = vertex_code.c_str();
    const char *fragment_code_c = fragment_code.c_str();

    unsigned int vertex_shader;
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_code_c, &vertex_size);
    glCompileShader(vertex_shader);
    print_iv_success(vertex_shader, GL_COMPILE_STATUS);

    unsigned int fragment_shader;
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_code_c, &fragment_size);
    glCompileShader(fragment_shader);
    print_iv_success(fragment_shader, GL_COMPILE_STATUS);

    ID = glCreateProgram();
    glAttachShader(ID, vertex_shader);
    glAttachShader(ID, fragment_shader);
    glLinkProgram(ID);
    print_iv_success(ID, GL_LINK_STATUS);

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
}

void Shader::print_iv_success(unsigned int shader, GLenum status) {
    int shaderSuccess;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &shaderSuccess);
    if (!shaderSuccess) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cout << status << " FAILED:\n" << infoLog << std::endl;
    }
}

void Shader::use() {
    glUseProgram(ID);
}

void Shader::setInt(std::string name, int value) const {
    glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
}

void Shader::setBool(std::string name, bool value) const {
    glUniform1i(glGetUniformLocation(ID, name.c_str()), (int) value);
}

void Shader::setFloat(std::string name, float value) const {
    glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
}

void Shader::setVec2(const std::string &name, const glm::vec2 &value) const {
    glUniform2fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}

void Shader::setVec2(const std::string &name, float x, float y) const {
    glUniform2f(glGetUniformLocation(ID, name.c_str()), x, y);
}

void Shader::setVec3(const std::string &name, const glm::vec3 &value) const {
    glUniform3fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}

void Shader::setVec3(const std::string &name, float x, float y, float z) const {
    glUniform3f(glGetUniformLocation(ID, name.c_str()), x, y, z);
}

void Shader::setVec4(const std::string &name, const glm::vec4 &value) const {
    glUniform4fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
}

void Shader::setVec4(const std::string &name, float x, float y, float z, float w) const {
    glUniform4f(glGetUniformLocation(ID, name.c_str()), x, y, z, w);
}

void Shader::setMat2(const std::string &name, const glm::mat2 &mat) const {
    glUniformMatrix2fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::setMat3(const std::string &name, const glm::mat3 &mat) const {
    glUniformMatrix3fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

void Shader::setMat4(const std::string &name, const glm::mat4 &mat) const {
    glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
