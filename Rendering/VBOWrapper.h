//
// Created by Horthrax on 8/19/2019.
//

#ifndef SWEETTRIPARCADE_VBOWRAPPER_H
#define SWEETTRIPARCADE_VBOWRAPPER_H

#include "../Block/Block.h"
#include <vector>
#include <GL/glew.h>

/*

			CubeAttribute(0, 0, 0,				0, 0,)
			CubeAttribute(0 + 1, 0, 0,			1, 0,)
			CubeAttribute(0 + 1,  0 + 1, 0,		1, 1,)
			CubeAttribute(0 + 1,  0 + 1, 0,		1, 1,)
			CubeAttribute(0, 0 + 1 , 0,		    0, 1,)
			CubeAttribute(0, 0, 0,			    0, 0,)

			CubeAttribute(0, 0,  0 + 1,			0, 0,)
			CubeAttribute(0 + 1, 0,  0 + 1,		1, 0,)
			CubeAttribute(0 + 1,  0 + 1,  0 + 1,  1, 1,)
			CubeAttribute(0 + 1,  0 + 1,  0 + 1,  1, 1,)
			CubeAttribute(0,  0 + 1,  0 + 1,      0, 1,)
			CubeAttribute(0, 0,  0+ 1,			0, 0,)

			CubeAttribute(0,  0 + 1,  0 + 1,		1, 1,)
			CubeAttribute(0,  0 + 1, 0,			0, 1,)
			CubeAttribute(0, 0, 0,				0, 0,)
			CubeAttribute(0, 0, 0,				0, 0,)
			CubeAttribute(0, 0,  0 + 1,			1, 0,)
			CubeAttribute(0,  0 + 1,  0 + 1,		1, 1,)

			CubeAttribute(0 + 1,  0 + 1,  0 + 1,  1, 1,)
			CubeAttribute(0 + 1,  0 + 1, 0,		0, 1,)
			CubeAttribute(0 + 1, 0, 0,			0, 0,)
			CubeAttribute(0 + 1, 0, 0,			0, 0,)
			CubeAttribute(0 + 1, 0,  0 + 1,		1, 0,)
			CubeAttribute(0 + 1,  0 + 1,  0 + 1,  1, 1,)

			CubeAttribute(0, 0, 0,				0, 0,)
			CubeAttribute(0 + 1, 0, 0,			1, 0,)
			CubeAttribute(0 + 1, 0,  0 + 1,		1, 1,)
			CubeAttribute(0 + 1, 0,  0 + 1,		1, 1,)
			CubeAttribute(0, 0, 0 + 1,			0, 1,)
			CubeAttribute(0, 0, 0,				0, 0,)

			CubeAttribute(0,  1, 0,			0, 0,)
			CubeAttribute(1,  1, 0,		1, 0,)
			CubeAttribute(1,  1, 1,  1, 1,)
			CubeAttribute(1,  1, 1,  1, 1,)
			CubeAttribute(0,  1, 1,		0, 1,)
			CubeAttribute(0,  1, 0,			0, 0,)
 *
 */

class CubeVBO
{
public:
    void generateVertices(){
        vertices = {
                CubeAttribute(0, 0, 0, 0, 0),
                CubeAttribute(1, 0, 0, 1, 0),
                CubeAttribute(1, 1, 0, 1, 1),
                CubeAttribute(1, 1, 0, 1, 1),
                CubeAttribute(0, 1, 0, 0, 1),
                CubeAttribute(0, 0, 0, 0, 0),

                CubeAttribute(0, 0, 1, 0, 0),
                CubeAttribute(1, 0, 1, 1, 0),
                CubeAttribute(1, 1, 1, 1, 1),
                CubeAttribute(1, 1, 1, 1, 1),
                CubeAttribute(0, 1, 1, 0, 1),
                CubeAttribute(0, 0, 1, 0, 0),

                CubeAttribute(0, 1, 1, 1, 1),
                CubeAttribute(0, 1, 0, 0, 1),
                CubeAttribute(0, 0, 0, 0, 0),
                CubeAttribute(0, 0, 0, 0, 0),
                CubeAttribute(0, 0, 1, 1, 0),
                CubeAttribute(0, 1, 1, 1, 1),

                CubeAttribute(1, 1, 1, 1, 1),
                CubeAttribute(1, 1, 0, 0, 1),
                CubeAttribute(1, 0, 0, 0, 0),
                CubeAttribute(1, 0, 0, 0, 0),
                CubeAttribute(1, 0, 1, 1, 0),
                CubeAttribute(1, 1, 1, 1, 1),

                CubeAttribute(0, 0, 0, 0, 0),
                CubeAttribute(1, 0, 0, 1, 0),
                CubeAttribute(1, 0, 1, 1, 1),
                CubeAttribute(1, 0, 1, 1, 1),
                CubeAttribute(0, 0, 1, 0, 1),
                CubeAttribute(0, 0, 0, 0, 0),

                CubeAttribute(0, 1, 0, 0, 0),
                CubeAttribute(1, 1, 0, 1, 0),
                CubeAttribute(1, 1, 1, 1, 1),
                CubeAttribute(1, 1, 1, 1, 1),
                CubeAttribute(0, 1, 1, 0, 1),
                CubeAttribute(0, 1, 0, 0, 0)
        };
    }
    std::vector<CubeAttribute> vertices;
    unsigned int VBO;
    unsigned int VAO;
    int size;

    CubeVBO() {
        generateVertices();
        create();
    };
    ~CubeVBO()
    {
    };
    void create()
    {
        size = vertices.size();
        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);
        glBindVertexArray(VAO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(CubeAttribute), &vertices[0], GL_STATIC_DRAW);

        // Maybe should be GL_TRUE

        // Positions
        glVertexAttribPointer(0, 3, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(CubeAttribute), nullptr);
        glEnableVertexAttribArray(0);

        // Texture UV
        glVertexAttribPointer(1, 2, GL_UNSIGNED_BYTE, GL_FALSE, sizeof(CubeAttribute), (void*)offsetof(CubeAttribute, texture));
        glEnableVertexAttribArray(1);
    }
    void remove()
    {

    }
};

#endif //SWEETTRIPARCADE_VBOWRAPPER_H
